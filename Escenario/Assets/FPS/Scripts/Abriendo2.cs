using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo2 : MonoBehaviour
{
    public Animator Puerta2;
    private void OnTriggerEnter(Collider other)
    {
        Puerta2.Play("OpenDoor1");
    }

    private void OnTriggerExit(Collider other)
    {
        Puerta2.Play("Cerrar1");
    }
}

