using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo7 : MonoBehaviour
{
    public Animator Puerta7;
    private void OnTriggerEnter(Collider other)
    {
        Puerta7.Play("OpenDoor6");
    }

    private void OnTriggerExit(Collider other)
    {
        Puerta7.Play("Cerrar6");
    }
}
