using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo4 : MonoBehaviour
{
    public Animator Puerta4;
    private void OnTriggerEnter(Collider other)
    {
        Puerta4.Play("OpenDoor3");
    }

    private void OnTriggerExit(Collider other)
    {
        Puerta4.Play("Cerrar3");
    }

}
