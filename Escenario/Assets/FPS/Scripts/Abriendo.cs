using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo : MonoBehaviour
{
   public Animator Puerta1;
   private void OnTriggerEnter(Collider other)
    {
        Puerta1.Play("OpenDoor");
    }

    private void OnTriggerExit(Collider other)
    {
        Puerta1.Play("Cerrar");
    }
}
