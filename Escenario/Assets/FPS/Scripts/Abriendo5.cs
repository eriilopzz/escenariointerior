using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo5 : MonoBehaviour
{
    public Animator Puerta5;
    private void OnTriggerEnter(Collider other)
    {
        Puerta5.Play("OpenDoor4");
    }

    private void OnTriggerExit(Collider other)
    {
        Puerta5.Play("Cerrar4");
    }
}
