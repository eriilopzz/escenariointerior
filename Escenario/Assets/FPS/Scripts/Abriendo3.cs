using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo3 : MonoBehaviour
{
    public Animator Puerta3;
    private void OnTriggerEnter(Collider other)
    {
        Puerta3.Play("OpenDoor2");
    }

    private void OnTriggerExit(Collider other)
    {
        Puerta3.Play("Cerrar2");
    }
}

