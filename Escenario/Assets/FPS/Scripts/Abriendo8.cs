using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo8 : MonoBehaviour
{
    public Animator Puerta8;
    private void OnTriggerEnter(Collider other)
    {
        Puerta8.Play("OpenDoor7");
    }

    private void OnTriggerExit(Collider other)
    {
        Puerta8.Play("Cerrar7");
    }
}