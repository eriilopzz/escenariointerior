using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo6 : MonoBehaviour
{
    public Animator Puerta6;
    private void OnTriggerEnter(Collider other)
    {
        Puerta6.Play("OpenDoor5");
    }

    private void OnTriggerExit(Collider other)
    {
        Puerta6.Play("Cerrar5");
    }
}
